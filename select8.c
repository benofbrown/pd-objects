/*
 * Copyright (c) 2021 Ben Brown
 * For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE" in this distribution.
 */

/*
 * select8 takes a float in to its first inlet which determines the currently
 * active outlet.
 *
 * Alternatively a bang can be sent to advance to the next outlet.
 *
 * The float `1` is sent to the active outlet, and `0` to the others.
 */

#include <m_pd.h>

static t_class *select8_class;

typedef struct _select8 {
  t_object x_obj;
  int active;
  t_outlet *outs[8];
} t_select8;

void select8_float(t_select8 *x, t_floatarg f) {
  x->active = (int) f;
  for (int i = 0; i < 8; i++) {
    outlet_float(x->outs[i], i == x->active ? 1 : 0);
  }
}

void select8_bang(t_select8 *x) {
  int active = x->active;
  select8_float(x, (active + 1) % 8);
}

void *select8_new(void) {
  t_select8 *x = (t_select8 *) pd_new(select8_class);
  for (int i = 0; i < 8; i++) {
    x->outs[i] = outlet_new(&x->x_obj, &s_float);
  }
  return (void *) x;
}

void select8_setup(void) {
  select8_class = class_new(gensym("select8"),
    (t_newmethod) select8_new, 0, sizeof(t_select8), CLASS_DEFAULT, 0);
  class_addbang(select8_class, select8_bang);
  class_addfloat(select8_class, select8_float);
}
