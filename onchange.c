/*
 * Copyright (c) 2021 Ben Brown
 * For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE" in this distribution.
 */

/*
 * onchange takes a float in to it's first output and sends a bang when it
 * receives a different value to the previous one.
 */

#include <m_pd.h>

static t_class *onchange_class;

typedef struct _onchange {
  t_object x_obj;
  int current;
} t_onchange;

void onchange_float(t_onchange *x, t_floatarg f) {
  if ((int) f == x->current) return;
  x->current = (int) f;
  outlet_bang(x->x_obj.ob_outlet);
}

void *onchange_new(void) {
  t_onchange *x = (t_onchange *) pd_new(onchange_class);
  x->current = 0;
  outlet_new(&x->x_obj, &s_bang);
  return (void *) x;
}

void onchange_setup(void) {
  onchange_class = class_new(gensym("onchange"),
    (t_newmethod) onchange_new, 0, sizeof(t_onchange), CLASS_DEFAULT, 0);
  class_addfloat(onchange_class, onchange_float);
}
