/*
 * Copyright (c) 2021 Ben Brown
 * For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE" in this distribution.
 */

/*
 * switch8 takes anything in to its first inlet and sends it out to the
 * currently active outlet.
 *
 * The currently active outlet can be set by sending a float to the second
 * inlet, see `switch8_test.pd` for an example usage.
 */

#include <m_pd.h>

static t_class *switch8_class;

typedef struct _switch8 {
  t_object x_obj;
  int active;
  t_outlet *outs[8];
} t_switch8;

void switch8_anything(t_switch8 *x, t_symbol *s, int argc, t_atom *argv) {
  outlet_anything(x->outs[x->active], s, argc, argv);
}

void switch8_setactive(t_switch8 *x, t_floatarg f) {
  x->active = (int) f % 8;
}

void *switch8_new(void) {
  t_switch8 *x = (t_switch8 *) pd_new(switch8_class);
  x->active = 0;
  for (int i = 0; i < 8; i++) {
    x->outs[i] = outlet_new(&x->x_obj, &s_anything);
  }
  inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("setactive"));
  return (void *) x;
}

void switch8_setup(void) {
  switch8_class = class_new(gensym("switch8"),
    (t_newmethod) switch8_new, 0, sizeof(t_switch8), CLASS_DEFAULT, 0);
  class_addanything(switch8_class, switch8_anything);
  class_addmethod(switch8_class, (t_method) switch8_setactive, gensym("setactive"), A_FLOAT, 0);
}
