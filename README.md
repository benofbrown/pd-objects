Pure Data objects
-----------------

Some objects I've written that I find useful and couldn't find online.

Building
--------

*At present I'm only supporting Linux*


This should be pretty simple to build, all you need is `make`, `gcc` (other compilers might work fine too, I've not tested them though) and the puredata headers and libraries. On Debian these can be installed via the `puredata-dev` package.

To build simply run:

    make

This will create various `.pd_linux` files, which you can copy in to somewhere on puredata's path, or simply in the same directory as your `.pd` files.

Objects
-------

- `on1` takes in a float and sends out a bang if the float is 1.
- `onchange` takes in a float and sends out a bang if the float is different to the currently stored value.
- `select8` takes in a float, and sends the float `1` to the corresponding outlet, and `0` to all other outlets. Can also take in a bang to move to the next outlet.
- `switch8` takes in anything to its first inlet and sends it out of one of the eight outlets. A float sent to the second inlet determines which outlet is used.
