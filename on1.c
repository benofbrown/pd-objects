/*
 * Copyright (c) 2021 Ben Brown
 * For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE" in this distribution.
 */

/*
 * on1 takes a float in to it's first output and sends a bang when it
 * is equal to 1.
 */

#include <m_pd.h>

static t_class *on1_class;

typedef struct _on1 {
  t_object x_obj;
} t_on1;

void on1_float(t_on1 *x, t_floatarg f) {
  if ((int) f != 1) return;
  outlet_bang(x->x_obj.ob_outlet);
}

void *on1_new(void) {
  t_on1 *x = (t_on1 *) pd_new(on1_class);
  outlet_new(&x->x_obj, &s_bang);
  return (void *) x;
}

void on1_setup(void) {
  on1_class = class_new(gensym("on1"),
    (t_newmethod) on1_new, 0, sizeof(t_on1), CLASS_DEFAULT, 0);
  class_addfloat(on1_class, on1_float);
}
