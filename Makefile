CC = gcc
CPPFLAGS += -Wall -pedantic -std=c99 -Werror -Wno-cast-function-type
CFLAGS += -DPD -O2 -funroll-loops -fomit-frame-pointer

all: switch8.pd_linux select8.pd_linux onchange.pd_linux on1.pd_linux

.SUFFIXES: .pd_linux

.c.pd_linux:
	$(CC) -fPIC $(CPPFLAGS) $(CFLAGS) -o $*.o -c $*.c
	$(LD) -shared -o $*.pd_linux $*.o
	strip --strip-unneeded $*.pd_linux
	rm $*.o

clean:
	rm -f *.o *.pd_linux

.PHONY: all clean
